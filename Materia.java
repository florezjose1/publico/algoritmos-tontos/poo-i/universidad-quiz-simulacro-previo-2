
import java.util.ArrayList;
/**
 * Escriba en lenguaje natural una descripcion de los objetos
 * que caben en el concepto de la clase Materia.
 * @author Desarrollador: (Jose Florez - florezjoserodolfo@gmail.com)
 * @author Calificador (Milton Jes&uacute;s Vera Contreras - miltonjesusvc@ufps.edu.co) 
 * @version Math.sin(Math.PI-Double.MIN_VALUE) :) 
 */
public class Materia
{
    protected ArrayList<Grupo> grupos;
    protected ArrayList<Inscripcion> inscripciones;
    protected String nombre;
    protected int maximoDeAlumnosPorGrupo;
    
    public Materia(){}

    public Materia(String nombre, int maximoDeAlumnosPorGrupo)
    {
        //COMPLETE
        this.nombre = nombre;
        this.maximoDeAlumnosPorGrupo = maximoDeAlumnosPorGrupo;
        this.inscripciones = new ArrayList<Inscripcion>();
    }//fin constructor

    public boolean inscribir(Alumno alumno)
    {
        boolean inscribio = false;
        //COMPLETE
        Inscripcion i = new Inscripcion(alumno, this);
        if(!alumno.tienePromedioCondicional() && !this.inscripciones.contains(i)){
            this.inscripciones.add(i);
            inscribio = true;
        }
        return inscribio;
    }//fin inscribir

    public int generarGrupos()
    {
        //COMPLETE      
        this.grupos = new ArrayList();
        int totalGrupos = this.inscripciones.size()  /  this.maximoDeAlumnosPorGrupo;

        int c = 0;
        for(int i=0; i<totalGrupos; i++){
            Grupo g = new Grupo(this);
            for(int j=0; j<this.maximoDeAlumnosPorGrupo; j++){
                g.agregarAlumno(this.inscripciones.get(c).getAlumno());
                c++;
            }
            this.grupos.add(g);
        }
        
        return totalGrupos;        
    }//fin generarGrupos

    /**Metodo de acceso a la propiedad nombre*/
    public String getNombre(){
        return this.nombre;
    }//end method getNombre

    /**Metodo de modificacion a la propiedad nombre*/
    public void setNombre(String nombre){
        this.nombre = nombre;
    }//end method setNombre

    /**Metodo de acceso a la propiedad maximoDeAlumnosPorGrupo*/
    public int getMaximoDeAlumnosPorGrupo(){
        return this.maximoDeAlumnosPorGrupo;
    }//end method getMaximoDeAlumnosPorGrupo

    /**Metodo de modificacion a la propiedad maximoDeAlumnosPorGrupo*/
    public void setMaximoDeAlumnosPorGrupo(int maximoDeAlumnosPorGrupo){
        this.maximoDeAlumnosPorGrupo = maximoDeAlumnosPorGrupo;
    }//end method setMaximoDeAlumnosPorGrupo
    
    /**Metodo de acceso a la propiedad inscripciones*/
    public java.util.ArrayList<Inscripcion> getInscripciones(){
        return this.inscripciones;
    }//end method getInscripciones
    
    /**Metodo de acceso a la propiedad grupos*/
    public java.util.ArrayList<Grupo> getGrupos(){
        return this.grupos;
    }//end method getInscripciones

    /***/
    public Grupo getGrupo(int numero)
    {
        return this.grupos.get(numero);
    }//fin getGrupo

    /**Compara el nombre de this con un String u otro Materia*/
    public boolean equals(Object other)
    {
        //COMPLETE
        boolean eq = false;
        if(other instanceof String) eq = this.nombre.equals(other);
        else if(other instanceof Materia) {
            Materia m = (Materia)(other);
            eq = this.nombre.equals(m.getNombre());
        }
        return eq;
    }//fin equals

}//fin class Materia
