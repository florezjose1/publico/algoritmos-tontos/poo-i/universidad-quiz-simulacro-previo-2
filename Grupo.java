
import java.util.ArrayList;
/**
 * Escriba en lenguaje natural una descripcion de los objetos
 * que caben en el concepto de la clase Grupo.
 * @author Desarrollador: (Jose Florez - florezjoserodolfo@gmail.com)
 * @author Calificador (Milton Jes&uacute;s Vera Contreras - miltonjesusvc@ufps.edu.co) 
 * @version Math.sin(Math.PI-Double.MIN_VALUE) :) 
 */
public class Grupo
{
    protected Alumno[] alumnos;
    protected Materia materia;
    protected int contadorDeAlumnos;
    
    public Grupo() {
    }

    public Grupo(Materia materia)
    {
        //COMPLETE
        this.materia = materia;
        this.alumnos = new Alumno[materia.getMaximoDeAlumnosPorGrupo()];
    }//fin construuctor

    public Grupo(Alumno[] alumnos)
    {
        //COMPLETE
        this.alumnos = alumnos;
    }//fin construuctor

    public Grupo(Materia materia, Alumno[] alumnos)
    {
        //COMPLETE
        this.materia = materia;
        this.alumnos = alumnos;
    }//fin construuctor

    public boolean agregarAlumno(Alumno alumno)
    {
        boolean puedeAgregar = false;
        //COMPLETE
        if(!this.contieneAlumno(alumno) && this.contadorDeAlumnos < this.alumnos.length) {
            // int p = 0;
            // while(this.alumnos[p]!=null) p++;
            this.alumnos[this.contadorDeAlumnos++] = alumno;
            puedeAgregar = true;
        }
        return puedeAgregar;
    }//fin agregarAlumno

    public boolean contieneAlumno(Alumno alumno){
        boolean existe = false; 
        for(Alumno a : this.alumnos) {
            if(a!=null && a.equals(alumno)) {
                existe = true;
                break;
            }
        }
       
        return existe;
    }

    /**Metodo de acceso a la propiedad Alumnos*/
    public Alumno[] getAlumnos(){
        return this.alumnos;
    }//end method getAlumnos

    /**Metodo de modificacion a la propiedad contadorDeAlumnos*/
    public void setContadorDeAlumnos(int contadorDeAlumnos){
        this.contadorDeAlumnos = contadorDeAlumnos;
    }//end method setContadorDeAlumnos

    /**Metodo de acceso a la propiedad contadorDeAlumnos*/
    public int getContadorDeAlumnos(){
        return this.contadorDeAlumnos;
    }//end method getContadorDeAlumnos

    /**Metodo de acceso a la propiedad materia*/
    public Materia getMateria(){
        return this.materia;
    }//end method getMateria

    /**Metodo de modificación a la propiedad materia*/
    public void setMateria(Materia materia){
        this.materia = materia;
    }//end method setMateria

}//fin class Grupo
