
import java.util.ArrayList;
/**
 * Escriba en lenguaje natural una descripcion de los objetos
 * que caben en el concepto de la clase Universidad.
 * @author Desarrollador: (Jose Florez - florezjoserodolfo@gmail.com)
 * @author Calificador (Milton Jes&uacute;s Vera Contreras - miltonjesusvc@ufps.edu.co) 
 * @version Math.sin(Math.PI-Double.MIN_VALUE) :) 
 */
public class Universidad
{
    public static final int MAX_ALUMNOS = 100;
    public static final int MAX_MATERIAS = 10;
    protected Materia [] materias;
    protected Alumno [] alumnos;
    protected int contadorMaterias;
    protected int contadorAlumnos;

    public Universidad()
    {
        //COMPLETE
        this.materias = new Materia[MAX_MATERIAS];
        this.alumnos = new Alumno[MAX_ALUMNOS];
    }//fin constructor

    public boolean agregarMateria(String nombre, int maximoDeAlumnosPorGrupo)
    {
        boolean agrego = false;
        Materia m = new Materia(nombre, maximoDeAlumnosPorGrupo);
        if(!this.contieneMateria(m) && this.contadorMaterias<this.materias.length) {
            this.materias[this.contadorMaterias++] = m;
            agrego = true;
        }
        //COMPLETE
        return agrego;
    }//fin agregarMateria

    public boolean contieneMateria(Materia materia){
        boolean existe = false;
        //COMPLETE
        for(Materia m : this.materias) {
            if(m!=null && m.equals(materia)){
                existe = true;
                break;
            }
        }
        return existe;
    }

    public boolean agregarAlumno(String codigo, String nombres, String apellidos, int edad, float promedio)
    {
        boolean agrego = false;
        Alumno a = new Alumno(codigo, nombres, apellidos, edad, promedio);
        if(!this.contieneAlumno(a) && this.contadorAlumnos<this.alumnos.length) {
            this.alumnos[this.contadorAlumnos++] = a;
            agrego = true;
        }
        return agrego;
    }//fin agregarAlumno

    public boolean contieneAlumno(Alumno alumno){
        boolean existe = false;
        //COMPLETE
        for(Alumno a : this.alumnos) {
            if(a!=null && a.equals(alumno)) {
                existe = true;
                break;
            }
        }
        return existe;
    }

    /**Regresa el alumno en la posicion numero o null si no existe*/
    public Alumno getAlumno(int numero)
    {
        //COMPLETE
        if(numero>0 && numero<=this.alumnos.length){
            numero--;
            return this.alumnos[numero];
        }
        return null;
    }//fin getAlumno

    /**Regresa el Materia en la posicion numero o null si no existe*/
    public Materia getMateria(int numero)
    {
        //COMPLETE
        if(numero>0 && numero<=this.materias.length){
            numero--;
            return this.materias[numero];
        }
        return null;
    }//fin getMateria      

    /**
     * Elimina el alumno en la posicion numero.
     * Regresa el objeto eliminado si existe o NULL si no existe.
     * Reacomoda el arreglo para que todos los elementos queden contiguos y no queden vacios
     */
    public Alumno eliminarAlumno(int numero){
        //COMPLETE
        Alumno a = null;
        if(numero>0 && numero<=this.alumnos.length) {
            numero--;
            a = this.alumnos[numero];
            if(a!=null){
                for(int i = numero; i<this.alumnos.length-1; i++) {
                    this.alumnos[i] = this.alumnos[i+1];
                }
                this.alumnos[this.alumnos.length-1] = null;
                this.contadorAlumnos--;
            }
        }
        return a;
    }

    /**
     * Elimina la materia en la posicion numero.
     * Regresa el objeto eliminado si existe o NULL si no existe.
     * Reacomoda el arreglo para que todos los elementos queden contiguos y no queden vacios
     */
    public Materia eliminarMateria(int numero){
        //COMPLETE
        Materia m = null;
        if(numero>0 && numero<=this.materias.length) {
            numero--;
            m = this.materias[numero];
            if(m!=null) {
                for(int i = numero; i<this.materias.length-1; i++) {
                    this.materias[i] = this.materias[i+1];
                }
                    this.materias[this.materias.length-1] = null;
                this.contadorMaterias--;
            }
        }
        return m;
    }
}//fin class Universidad
